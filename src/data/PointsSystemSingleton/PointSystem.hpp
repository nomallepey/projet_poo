#pragma once

namespace NomCool::data::PointSystemSingleton
{
    class PointSystem
    {

    protected:
        PointSystem();

        static PointSystem* mSingleton;

        int mTotalPoints = 0;

    public:

        /**
         * Singletons should not be cloneable.
         */
        PointSystem(PointSystem &other) = delete;
        /**
         * Singletons should not be assignable.
         */
        void operator=(const PointSystem &) = delete;
        /**
         * This is the static method that controls the access to the singleton
         * instance. On the first run, it creates a singleton object and places it
         * into the static field. On subsequent runs, it returns the client existing
         * object stored in the static field.
         */

        static PointSystem *GetInstance();

        void PlayerWin();
        void PlayerLose();

        int TotalPoints(); 
    };

}