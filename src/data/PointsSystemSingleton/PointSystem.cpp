#include "PointSystem.hpp"

namespace NomCool::data::PointSystemSingleton{
    PointSystem::PointSystem(){}

    PointSystem* PointSystem::mSingleton = nullptr;

    PointSystem * PointSystem::GetInstance(){
        if(mSingleton==nullptr){
            mSingleton = new PointSystem();
        }
        return mSingleton;
    }

    void PointSystem::PlayerWin(){
        mTotalPoints += 1;
    }
    void PointSystem::PlayerLose(){
        mTotalPoints -= 1;
    }

    int PointSystem::TotalPoints(){
        return mTotalPoints;
    }; 
}



