#include "Soustraction.hpp"
#include <iostream>

namespace NomCool::data::OperationsStrategy
{
    Soustraction::Soustraction(int num1, int num2) : OperationStrategy(num1, num2)
    {
    }

    int Soustraction::getResult() const
    {
        return mNum1 - mNum2;
    }

    std::string Soustraction::getExpression() const
    {
        return std::to_string(mNum1) + " - " + std::to_string(mNum2);
    }
}