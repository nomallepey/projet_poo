#pragma once
#include "OperationStrategy.hpp"


namespace NomCool::data::OperationsStrategy
{
    class Addition : public OperationStrategy
    {
    public:
        Addition(int num1, int num2);
        int getResult() const override;
        std::string getExpression() const override;
    };
}