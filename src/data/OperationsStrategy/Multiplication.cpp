#include "Multiplication.hpp"
#include <iostream>

namespace NomCool::data::OperationsStrategy
{
    Multiplication::Multiplication(int num1, int num2) : OperationStrategy(num1, num2)
    {
    }

    int Multiplication::getResult() const
    {
        return mNum1 * mNum2;
    }

    std::string Multiplication::getExpression() const
    {
        return std::to_string(mNum1) + " * " + std::to_string(mNum2);
    }
}