#include "Addition.hpp"
#include <iostream>

namespace NomCool::data::OperationsStrategy
{
    Addition::Addition(int num1, int num2) : OperationStrategy(num1, num2)
    {
    }

    int Addition::getResult() const
    {
        return mNum1 + mNum2;
    }

    std::string Addition::getExpression() const
    {
        return std::to_string(mNum1) + " + " + std::to_string(mNum2);
    }

}