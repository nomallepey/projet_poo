#pragma once
#include "OperationStrategy.hpp"

namespace NomCool::data::OperationsStrategy
{
    class Soustraction : public OperationStrategy
    {
    public:
        Soustraction(int num1, int num2);
        int getResult() const override;
        std::string getExpression() const override;
    };
}