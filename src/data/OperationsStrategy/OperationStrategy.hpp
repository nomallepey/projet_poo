
#pragma once
#include <string>

namespace NomCool::data::OperationsStrategy
{
class OperationStrategy
    {
        public:
            OperationStrategy(int num1, int num2);
            virtual ~OperationStrategy() = default;
            virtual int getResult() const = 0;
            virtual std::string getExpression() const = 0;

        protected:
            int mNum1;
            int mNum2;

    };
}