#pragma once
#include "OperationStrategy.hpp"

namespace NomCool::data::OperationsStrategy
{
    class Multiplication : public OperationStrategy
    {
    public:
        Multiplication(int num1, int num2);
        int getResult() const override;
        std::string getExpression() const override;
    };
}
