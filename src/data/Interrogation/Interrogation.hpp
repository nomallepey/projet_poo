
#pragma once

#include <string>
#include <utility>
#include <vector>
#include "Utils.hpp"
#include "IInterrogation.hpp"
#include "../OperationsStrategy/Multiplication.hpp"
#include "../OperationsStrategy/Addition.hpp"
#include "../OperationsStrategy/Soustraction.hpp"
#include "../OperationsStrategy/OperationStrategy.hpp"

namespace NomCool::data {

using Response = std::string;

class Interrogation : public IInterrogation
{
public:
	virtual ~Interrogation() = default;
	Interrogation(int lhs, int rhs);
	// The question to ask
	std::string question() override;
	// The list of possible answer with the response to send back attached
	std::vector<std::pair<std::string, Response>> availableAnswers() override;
	
private:
	void setRandomQuestion();
	NomCool::data::OperationsStrategy::OperationStrategy* getRandomOperation(int lhs, int rhs);
	std::string mQuestion;
	std::vector<std::pair<std::string, Response>> mProposedAnswers;
	int mLhs;
	int mRhs;
};

}
