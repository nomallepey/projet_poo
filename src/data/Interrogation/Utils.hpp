#pragma once
#include <random>


namespace NomCool::data {

    class Utils {
        public:
            int getRandomNumber(int min, int max) const{
                std::uniform_int_distribution<> dis(min, max); 
                std::random_device rd;
                std::mt19937 gen(rd());

                return dis(gen);
            }
    };
}