#pragma once
#include <memory>
#include <data/Interrogation/IInterrogation.hpp>
#include <data/Interrogation/Interrogation.hpp>
#include <data/PointsSystemSingleton/PointSystem.hpp>
#include <data/DifficultyInterrogationFactory/InterrogationFactory.hpp>
#include <data/DifficultyInterrogationFactory/EasyInterrogationFactory.hpp>
#include <data/DifficultyInterrogationFactory/MediumInterrogationFactory.hpp>
#include <data/DifficultyInterrogationFactory/HardInterrogationFactory.hpp>

#include <stdexcept>

namespace NomCool::data {

    class InterrogationManager
    {
        public:
            virtual ~InterrogationManager() {}
            InterrogationManager();
            IInterrogation* getInterrogation(std::string difficulty, std::string typeInterrogation);
        private:
            std::unique_ptr<IInterrogation> mInterrogation;
            std::unique_ptr<InterrogationFactory> mfactory;
    };

}
