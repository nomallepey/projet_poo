
#include "Interrogation.hpp"
#include <random>

#include <iostream>
#include "Utils.hpp"

#define DEBUG_MSG(msg) std::cerr << "Debug message [" << __LINE__ << "]: " << msg << std::endl
namespace NomCool::data {

Interrogation::Interrogation(int lhs, int rhs): mLhs(lhs), mRhs(rhs)
{
}



data::OperationsStrategy::OperationStrategy* Interrogation::getRandomOperation(int lhs, int rhs){

	NomCool::data::Utils utils;
    int choice = utils.getRandomNumber(0, 2);

	switch (choice)
	{
		case 0: return new data::OperationsStrategy::Multiplication(lhs, rhs);
		case 1: return new data::OperationsStrategy::Addition(lhs, rhs);
		case 2: return new data::OperationsStrategy::Soustraction(lhs, rhs);
		default: return new data::OperationsStrategy::Multiplication(lhs, rhs);
	}
}

void Interrogation::setRandomQuestion()
{
	NomCool::data::Utils utils;
	auto lhs = utils.getRandomNumber(mLhs, mRhs);
	auto rhs =  utils.getRandomNumber(1, mRhs+1);

	auto valuePlus1 = [this](int value)
	{
		return value % mRhs + 1;
	};

	data::OperationsStrategy::OperationStrategy* operation = getRandomOperation(lhs, rhs);

	mQuestion = operation->getExpression();

	mProposedAnswers = 	{
		{std::to_string(operation->getResult()), "Correct"},
		{std::to_string(lhs*valuePlus1(rhs)), "Incorrect"},
		{std::to_string(valuePlus1(lhs)*rhs), "Incorrect"},
		{"I don't know", "I don't know"}
	};

}

std::string Interrogation::question()
{
	setRandomQuestion();
	return mQuestion;
}

std::vector<std::pair<std::string, Response>> Interrogation::availableAnswers()
{
	return mProposedAnswers;
}

} // namespace NomCool::data
