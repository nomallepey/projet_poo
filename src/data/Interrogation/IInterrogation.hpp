
#pragma once

#include <string>
#include <utility>
#include <vector>

namespace NomCool::data {

using Response = std::string;

class IInterrogation
{
public:
	// The question to ask
	virtual std::string question() = 0;
	// The list of possible answer with the response to send back attached
	virtual std::vector<std::pair<std::string, Response>> availableAnswers() = 0;
};

}
