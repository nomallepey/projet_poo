#include "InterrogationDecorateurSquareRoot.hpp"
#include <iostream> 
#include <stdexcept>
#include <cmath>

  #define DEBUG_MSG(msg) std::cerr << "Debug message [" << __LINE__ << "]: " << msg << std::endl

namespace NomCool::data
{

InterrogationDecorateurSquareRoot::InterrogationDecorateurSquareRoot(IInterrogation* interrogation): mInterrogation(interrogation)
  {
  }

  std::string InterrogationDecorateurSquareRoot::question()
  {
    return "√( " + mInterrogation->question() + " ) ";
  }

 std::vector<std::pair<std::string, Response>> InterrogationDecorateurSquareRoot::availableAnswers(){
    std::vector<std::pair<std::string, NomCool::data::Response>> copy;
    for (const auto& pair : mInterrogation->availableAnswers()) {
        std::string firstElementAsInt;
        try {
            firstElementAsInt = std::to_string( std::sqrt(std::stoi(pair.first)));
        } catch (std::invalid_argument&) {
            firstElementAsInt = "";
        }
        copy.push_back({firstElementAsInt, pair.second});
    }
    return copy;
}
}


