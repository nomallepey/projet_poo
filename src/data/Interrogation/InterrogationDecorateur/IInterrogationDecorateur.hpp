#pragma once

#include "../Utils.hpp"
#include "../IInterrogation.hpp" // Add the missing include statement

namespace NomCool::data
{
   class IInterrogationDecorateur : public IInterrogation {   
      protected:
         NomCool::data::IInterrogation* mInterrogation;

      public:
         virtual ~IInterrogationDecorateur() {}
   };    
}