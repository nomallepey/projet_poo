#pragma once
#include "IInterrogationDecorateur.hpp"
#include "../IInterrogation.hpp"

#include "../Utils.hpp"

namespace NomCool::data
{
   class InterrogationDecorateurPower : public IInterrogationDecorateur {
      protected:
         NomCool::data::IInterrogation* mInterrogation;
      private: 
         int mExponentielle;
      public:
         InterrogationDecorateurPower(NomCool::data::IInterrogation* interrogation);
         std::string question() override;
         std::vector<std::pair<std::string, Response>> availableAnswers() override;

   };
}