#include "InterrogationDecorateurPower.hpp"
#include <iostream> 
#include <stdexcept>
#include <cmath>

#define DEBUG_MSG(msg) std::cerr << "Debug message [" << __LINE__ << "]: " << msg << std::endl

namespace NomCool::data
{

  InterrogationDecorateurPower::InterrogationDecorateurPower(NomCool::data::IInterrogation* interrogation): mInterrogation(interrogation)
  {
    mExponentielle = 2;
  }

  std::string InterrogationDecorateurPower::question()
  {
    return "( " + mInterrogation->question() + " ) ** " + std::to_string(mExponentielle);
  }

 std::vector<std::pair<std::string, Response>> InterrogationDecorateurPower::availableAnswers(){
    std::vector<std::pair<std::string, NomCool::data::Response>> copy;
    for (const auto& pair : mInterrogation->availableAnswers()) {
        std::string firstElementAsInt;
        try {
            if (pair.first != "I don't know") {
              firstElementAsInt = std::to_string( std::pow(std::stoi(pair.first), mExponentielle));
            }else{
              firstElementAsInt = pair.first;
            }
        } catch (std::invalid_argument&) {
            firstElementAsInt = "";
        }
        copy.push_back({firstElementAsInt, pair.second});
    }
    return copy;
}
}


