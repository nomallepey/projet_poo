#include "InterrogationManager.hpp"



#define DEBUG_MSG(msg) std::cerr << "Debug message [" << __LINE__ << "]: " << msg << std::endl
namespace NomCool::data {

    InterrogationManager::InterrogationManager()
    {
    }

    IInterrogation* InterrogationManager::getInterrogation(std::string difficulty, std::string typeInterrogation) {
        if (difficulty == "Easy") {
            mfactory = std::make_unique<NomCool::data::EasyInterrogationFactory>();
        } else if (difficulty == "Medium") {
            mfactory = std::make_unique<NomCool::data::MediumInterrogationFactory>();
        } else if (difficulty == "Hard") {
            mfactory = std::make_unique<NomCool::data::HardInterrogationFactory>();
        } else {
            throw std::invalid_argument("Unknown difficulty level");
        }

        if (typeInterrogation == "Simple") {
            mInterrogation = mfactory->createInterrogationSimple();
        } else if (typeInterrogation == "Power") {
            mInterrogation = mfactory->createInterrogationPower();
        } else if (typeInterrogation == "Root") {
            mInterrogation = mfactory->createInterrogationRoot();
        } else {
            throw std::invalid_argument("Unknown type of question");
        }
        return mInterrogation.get();
    }

}

