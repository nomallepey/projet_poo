
#pragma once 
#include "data/Interrogation/IInterrogation.hpp"
#include "data/Interrogation/InterrogationDecorateur/InterrogationDecorateurPower.hpp"
#include "data/Interrogation/InterrogationDecorateur/InterrogationDecorateurSquareRoot.hpp"
#include <memory>

namespace NomCool::data {
    class InterrogationFactory {
    public:
        virtual ~InterrogationFactory(){};
        virtual std::unique_ptr<NomCool::data::IInterrogation> createInterrogationSimple() const = 0;
        std::unique_ptr<NomCool::data::IInterrogation> createInterrogationPower() const;
        std::unique_ptr<NomCool::data::IInterrogation> createInterrogationRoot() const;
    };
}
