
#include "data/DifficultyInterrogationFactory/InterrogationFactory.hpp"

namespace NomCool::data {

    std::unique_ptr<NomCool::data::IInterrogation> InterrogationFactory::createInterrogationPower() const {
            return std::make_unique<InterrogationDecorateurPower>(createInterrogationSimple().release());
    }

    std::unique_ptr<NomCool::data::IInterrogation> InterrogationFactory::createInterrogationRoot() const{
        return std::make_unique<InterrogationDecorateurSquareRoot>(createInterrogationSimple().release());
    }
}
