#pragma once
#include <memory>
#include "data/DifficultyInterrogationFactory/InterrogationFactory.hpp"
#include "data/Interrogation/Interrogation.hpp"
#include "data/Interrogation/InterrogationDecorateur/InterrogationDecorateurPower.hpp"

namespace NomCool::data {
    class MediumInterrogationFactory : public NomCool::data::InterrogationFactory {
        public:
            std::unique_ptr<NomCool::data::IInterrogation> createInterrogationSimple() const override;
    };
}