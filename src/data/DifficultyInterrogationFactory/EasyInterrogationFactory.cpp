#include "EasyInterrogationFactory.hpp"

namespace NomCool::data {

    std::unique_ptr<NomCool::data::IInterrogation> EasyInterrogationFactory::createInterrogationSimple() const{
        return std::make_unique<NomCool::data::Interrogation>(1, 10);
    }
};