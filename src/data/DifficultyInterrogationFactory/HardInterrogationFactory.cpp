#include "HardInterrogationFactory.hpp"

namespace NomCool::data {
    std::unique_ptr<IInterrogation> HardInterrogationFactory::createInterrogationSimple() const {
        return std::make_unique<NomCool::data::Interrogation>(20, 100);
    }
};