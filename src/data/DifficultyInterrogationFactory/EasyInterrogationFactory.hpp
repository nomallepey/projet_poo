#pragma once
#include <memory>
#include "data/DifficultyInterrogationFactory/InterrogationFactory.hpp"
#include "data/Interrogation/Interrogation.hpp"



namespace NomCool::data {
    class EasyInterrogationFactory : public NomCool::data::InterrogationFactory {
        public:
            std::unique_ptr<NomCool::data::IInterrogation> createInterrogationSimple() const override;
    };
}