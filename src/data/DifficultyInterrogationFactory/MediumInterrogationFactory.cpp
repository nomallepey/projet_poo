#include "MediumInterrogationFactory.hpp"

namespace NomCool::data {
     std::unique_ptr<IInterrogation> MediumInterrogationFactory::createInterrogationSimple() const {
        return std::make_unique<NomCool::data::Interrogation>(10, 20);

    }
};