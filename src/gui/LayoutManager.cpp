#include "LayoutManager.hpp"
#include <iostream>

namespace NomCool::gui {

LayoutManager::LayoutManager()
    : inactivityTimer(new QTimer(this)),
      countdownTimer(new QTimer(this)),
      timerLabel(new QLabel("Time remaining: 20 seconds", &mMainWindow)),
      timeRemaining(60)
{
    
    // Timer label setup
    mMainWindow.AddTopWidgetToLayout(timerLabel);
    
    // Button for random question
    auto* randomQuestion = new QPushButton("Random question");
    connect(randomQuestion, &QPushButton::clicked, this, &LayoutManager::updateInterrogation);
    mMainWindow.AddWidgetToLayout(randomQuestion, 1, 0);

    // Difficulty widget
    auto* difficulty = new Difficulty();
    connect(difficulty, &Difficulty::responseSelected, &mMainWindow, &MainWindow::updateDifficulty);
    connect(difficulty, &Difficulty::responseSelected, this, &LayoutManager::updateInterrogation);

    mMainWindow.AddWidgetToLayout(difficulty, 2, 0);

    // TypeInterrogation widget
    auto* typeInterrogation = new TypeInterrogation();
    connect(typeInterrogation, &TypeInterrogation::responseSelected, &mMainWindow, &MainWindow::updateTypeInterrogation);
    connect(typeInterrogation, &TypeInterrogation::responseSelected, this, &LayoutManager::updateInterrogation);
    mMainWindow.AddWidgetToLayout(typeInterrogation, 3, 0);
    updateInterrogation();


    // Handle responses
    connect(&mMainWindow, &MainWindow::responseSelected, this, [this](data::Response response) {
        auto* pointSystem = data::PointSystemSingleton::PointSystem::GetInstance();

        if (response == "I don't know") {
            pointSystem->PlayerLose();
            mMainWindow.setPreviousResult({data::Result::Status::Failure, "You will get it next time!"});
        } else if (response == "Correct") {
            pointSystem->PlayerWin();
            mMainWindow.setPreviousResult({data::Result::Status::Success, "Well done!"});
        } else {
            pointSystem->PlayerLose();
            mMainWindow.setPreviousResult({data::Result::Status::Failure, "No, it's not that one!"});
        }
        updateInterrogation();
    });

    // Quit button
    auto* quitButton = new QPushButton("Quit");
    connect(quitButton, &QPushButton::clicked, &mMainWindow, &MainWindow::close);
    mMainWindow.AddDownWidgetToLayout(quitButton);
    connect(quitButton, &QPushButton::clicked, this, &LayoutManager::resetInactivityTimer);

    // Setup inactivity timer
    inactivityTimer->setInterval(21000);
    connect(inactivityTimer, &QTimer::timeout, this, &LayoutManager::handleInactivity);
        // Setup countdown timer to update label every second
    countdownTimer->setInterval(1000); // 1 second in milliseconds
    connect(countdownTimer, &QTimer::timeout, this, &LayoutManager::updateTimerLabel);


    // Start the inactivity timer
    inactivityTimer->start();
    countdownTimer->start();
}

void LayoutManager::updateTimerLabel() {
    if (timeRemaining > 0) {
        timeRemaining--;
        timerLabel->setText(QString("Time remaining: %1 seconds").arg(timeRemaining));
    }
}

void LayoutManager::resetInactivityTimer() {
    timeRemaining = 21; // Reset the time remaining
    inactivityTimer->start(); // Restart the inactivity timer
    updateTimerLabel(); // Update the label immediately
}

void LayoutManager::updateInterrogation() {
    mMainWindow.setInterrogation();
    resetInactivityTimer();
}

void LayoutManager::handleInactivity() {
    auto* pointSystem = data::PointSystemSingleton::PointSystem::GetInstance();
    pointSystem->PlayerLose();
    mMainWindow.setPreviousResult({data::Result::Status::Failure, "Too late... Be quicker next time!"});
    updateInterrogation();
}

void LayoutManager::show() {
    mMainWindow.show();
    resetInactivityTimer(); // Ensure the timer is running when the window is shown
}

}
