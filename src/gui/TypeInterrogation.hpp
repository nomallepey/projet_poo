
#pragma once
#include <memory>
#include <QWidget>
#include "data/Interrogation/InterrogationManager.hpp"
#include <string>
#include <vector>

namespace NomCool::gui {

class TypeInterrogation : public QWidget
{
Q_OBJECT

public:
	TypeInterrogation();

Q_SIGNALS:
	void responseSelected(data::Response response);
};

} // namespace NomCool::gui
