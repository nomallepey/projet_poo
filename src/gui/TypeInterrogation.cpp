
#include <iostream>
#include "TypeInterrogation.hpp"

#include <QGridLayout>
#include <QLabel>
#include <QPushButton>

#define DEBUG_MSG(msg) std::cerr << "Debug message [" << __LINE__ << "]: " << msg << std::endl


namespace NomCool::gui {

	TypeInterrogation::TypeInterrogation()
	{

		auto* mainLayout = new QVBoxLayout();
		auto* ChosingTypeInterrogation = new QLabel();
		ChosingTypeInterrogation->setText(QString::fromStdString("Choose the Type of Interrogation (default is simple):"));
		mainLayout->addWidget(ChosingTypeInterrogation);
		auto* typeInterrogationLayout = new QHBoxLayout();

       
    auto* typeInterrogationSimple = new QPushButton("Simple");
	connect(typeInterrogationSimple, &QPushButton::clicked, this, [this]
		{
			emit responseSelected("Simple");
		}
	);

	auto* typeInterrogationPower = new QPushButton("Power");
	connect(typeInterrogationPower, &QPushButton::clicked, this, [this]
		{
			emit responseSelected("Power");
		}
	);

	auto* typeInterrogationRoot = new QPushButton("Root");
	connect(typeInterrogationRoot, &QPushButton::clicked, this, [this]
		{
			emit responseSelected("Root");
		}
	);

	typeInterrogationLayout->addWidget(typeInterrogationSimple);
	typeInterrogationLayout->addWidget(typeInterrogationPower);
	typeInterrogationLayout->addWidget(typeInterrogationRoot);

    mainLayout->addLayout(typeInterrogationLayout);
	setLayout(mainLayout);

	}
} // namespace NomCool::gui




 