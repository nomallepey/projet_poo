
#include "PreviousResult.hpp"

#include <QHBoxLayout>
#include <QLabel>

namespace NomCool::gui {

PreviousResult::PreviousResult(const data::Result& result)
{
	auto* layout = new QHBoxLayout();
	QIcon resultIcon(result.isSuccess() ? ":/icons/ok.png" : ":/icons/ko.png");
	QLabel* iconLabel = new QLabel();
	iconLabel->setPixmap(resultIcon.pixmap(32, 32));
	iconLabel->setAlignment(Qt::AlignCenter);
	layout->addWidget(iconLabel);
	auto* text = new QLabel();
	data::PointSystemSingleton::PointSystem* pointSystem = data::PointSystemSingleton::PointSystem::GetInstance();
	text->setText(QString::fromStdString(result.message() + "\nNumber of points: " + std::to_string(pointSystem->TotalPoints()) ));
	layout->addWidget(text);
	setLayout(layout);
}

} // namespace NomCool::gui
