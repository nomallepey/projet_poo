
#pragma once
#include <memory>
#include <QWidget>
#include "data/Interrogation/InterrogationManager.hpp"
#include <string>
#include <vector>
#include <algorithm>
#include <QVBoxLayout>

#include <random>

namespace NomCool::gui {

class Interrogation : public QWidget
{
Q_OBJECT

public:
	Interrogation(std::string difficulty, std::string typeInterrogation);
private:
	std::vector<std::pair<std::string, NomCool::data::Response>> shuffleAnswersExceptLast(const std::vector<std::pair<std::string, NomCool::data::Response>>& availableAnswers);

Q_SIGNALS:
	void responseSelected(data::Response response);
};

} // namespace NomCool::gui
