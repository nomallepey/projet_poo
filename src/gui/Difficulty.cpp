
#include <iostream>
#include "Difficulty.hpp"

#include <QGridLayout>
#include <QLabel>
#include <QPushButton>

#define DEBUG_MSG(msg) std::cerr << "Debug message [" << __LINE__ << "]: " << msg << std::endl


namespace NomCool::gui {

	Difficulty::Difficulty()
	{

		auto* mainLayout = new QVBoxLayout();
		auto* ChosingDifficulty = new QLabel();
		ChosingDifficulty->setText(QString::fromStdString("Choose the difficulty (default is easy):"));
		mainLayout->addWidget(ChosingDifficulty);
		auto* difficultyLayout = new QHBoxLayout();

       
    auto* difficultyEasy = new QPushButton("Easy");
	connect(difficultyEasy, &QPushButton::clicked, this, [this]
		{
			emit responseSelected("Easy");
		}
	);

	auto* difficultyMedium = new QPushButton("Medium");
	connect(difficultyMedium, &QPushButton::clicked, this, [this]
		{
			emit responseSelected("Medium");
		}
	);

	auto* difficultyHard = new QPushButton("Hard");
	connect(difficultyHard, &QPushButton::clicked, this, [this]
		{
			emit responseSelected("Hard");
		}
	);

	difficultyLayout->addWidget(difficultyEasy);
	difficultyLayout->addWidget(difficultyMedium);
	difficultyLayout->addWidget(difficultyHard);

    mainLayout->addLayout(difficultyLayout);
	setLayout(mainLayout);

	}
} // namespace NomCool::gui




 