
#pragma once
#include <memory>
#include <QWidget>
#include "data/Interrogation/InterrogationManager.hpp"
#include <string>
#include <vector>

namespace NomCool::gui {

class Difficulty : public QWidget
{
Q_OBJECT

public:
	Difficulty();

Q_SIGNALS:
	void responseSelected(data::Response response);
};

} // namespace NomCool::gui
