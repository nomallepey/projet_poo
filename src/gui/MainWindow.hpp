
#pragma once

#include <QMainWindow>

#include <gui/Interrogation.hpp>
#include <gui/Difficulty.hpp>

#include <gui/PreviousResult.hpp>
#include <data/PointsSystemSingleton/PointSystem.hpp>

#include <QGridLayout>
#include <QLabel>

#include <string>
#include <vector>

namespace NomCool::data {
class Interrogation;
} // namespace NomCool::data

namespace NomCool::gui {

class MainWindow : public QMainWindow
{
Q_OBJECT

public:
	MainWindow();
	void AddWidgetToLayout(QWidget* widget, int row, int column);
	void AddTopWidgetToLayout(QWidget* widget);
	void AddDownWidgetToLayout(QWidget* widget);
	void AddPreviousResultWidgetToLayout(QWidget* widget);
	void AddCentralWidgetToLayout(QWidget* widget);
	void setInterrogation();
	void setPreviousResult(const data::Result& result);
	void updateDifficulty(NomCool::data::Response response);
	void updateTypeInterrogation(NomCool::data::Response response);


Q_SIGNALS:
	void responseSelected(data::Response response);

private:
	QGridLayout* mMainLayout = nullptr;
	std::pair<int, int> mInterrogationPosition;
	Interrogation* mInterrogation = nullptr;
	std::pair<int, int> mPreviousResultPosition;
	PreviousResult* mPreviousResult = nullptr;
	std::string mDifficulty = "Easy";
	std::string mTypeInterrogation = "Simple";
};
}