
#include "MainWindow.hpp"
#include <iostream>

#define DEBUG_MSG(msg) std::cerr << "Debug message [" << __LINE__ << "]: " << msg << std::endl
#include <gui/Interrogation.hpp>

#include <data/Interrogation/Interrogation.hpp>

#include <QVBoxLayout>

// Debug
#include <QPushButton>
#include <random>
// End debug

namespace NomCool::gui {


void MainWindow::AddWidgetToLayout(QWidget* widget, int row, int column)
{
	mMainLayout->addWidget(widget, row, column);
}

void MainWindow::AddTopWidgetToLayout(QWidget* widget)
{
	AddWidgetToLayout(widget, 0,0);
}

void MainWindow::AddCentralWidgetToLayout(QWidget* widget)
{
	mInterrogationPosition = {mPreviousResultPosition.first + 3, 0};
	AddWidgetToLayout(widget, mInterrogationPosition.first, mInterrogationPosition.second);
}

void MainWindow::AddPreviousResultWidgetToLayout(QWidget* widget)
{
	mPreviousResultPosition = {mMainLayout->rowCount(), 0};
	AddWidgetToLayout(widget, mPreviousResultPosition.first, mPreviousResultPosition.second);
}
void MainWindow::AddDownWidgetToLayout(QWidget* widget)
{
	AddWidgetToLayout(widget, mInterrogationPosition.first + 4, 0);
}

MainWindow::MainWindow()
{
	mMainLayout = new QGridLayout();

	// This row is reserved for the previous result widget
	// The next one should be added at the mPreviousResultPosition.first + 1
	mPreviousResultPosition = {mMainLayout->rowCount(), 0};

	// This row is reserved for the interrogation widget
	// The next one should be added at the mInterrogationPosition.first + 1
	mInterrogationPosition = {mPreviousResultPosition.first + 1, 0};


	auto* centralWidget = new QWidget();
	centralWidget->setLayout(mMainLayout);
	setCentralWidget(centralWidget);
}

void MainWindow::updateDifficulty(NomCool::data::Response response) {
        mDifficulty = response;   
}

void MainWindow::updateTypeInterrogation(NomCool::data::Response response) {
        mTypeInterrogation = response;   
}


void MainWindow::setInterrogation()
{
	if (mInterrogation)
	{
		mMainLayout->removeWidget(mInterrogation);
		delete mInterrogation;
	}	
	mInterrogation = new Interrogation(mDifficulty, mTypeInterrogation);
	connect(mInterrogation, &Interrogation::responseSelected, this, &MainWindow::responseSelected);
	AddCentralWidgetToLayout(mInterrogation);
}

void MainWindow::setPreviousResult(const data::Result& result)
{
	if (mPreviousResult)
	{
		mMainLayout->removeWidget(mPreviousResult);
		delete mPreviousResult;
	}	
	mPreviousResult = new PreviousResult(result);
	AddPreviousResultWidgetToLayout(mPreviousResult);
}

} // namespace NomCool::FenetrePrincipale

