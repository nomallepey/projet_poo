#pragma once

#include <QPushButton>
#include <QMainWindow>
#include <QObject>
#include <QTimer>
#include <QLabel>
#include "Difficulty.hpp"
#include "TypeInterrogation.hpp"
#include "MainWindow.hpp"

namespace NomCool::gui {

class LayoutManager : public QObject {
    Q_OBJECT
public:
    LayoutManager();
    virtual ~LayoutManager(){};    
    void show();

private slots:
    void resetInactivityTimer();
    void handleInactivity();
    void updateInterrogation();
    void updateTimerLabel();

private:
    NomCool::gui::MainWindow mMainWindow;
    QTimer* inactivityTimer;
    QTimer* countdownTimer;
    QLabel* timerLabel;
    int timeRemaining;
};

}
