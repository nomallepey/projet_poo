
#include <iostream>
#include "Interrogation.hpp"

#include <QGridLayout>
#include <QLabel>
#include <QPushButton>

#define DEBUG_MSG(msg) std::cerr << "Debug message [" << __LINE__ << "]: " << msg << std::endl


namespace NomCool::gui {

	Interrogation::Interrogation(std::string difficulty, std::string typeInterrogation)
	{
		NomCool::data::InterrogationManager interrogationManager;
		NomCool::data::IInterrogation* interrogation = interrogationManager.getInterrogation(difficulty, typeInterrogation);
		
		auto* mainLayout = new QVBoxLayout();
		auto* questionLabel = new QLabel();
		questionLabel->setText(QString::fromStdString(interrogation->question()));
		mainLayout->addWidget(questionLabel);
		auto* solutionLayout = new QGridLayout();

		auto solutions = shuffleAnswersExceptLast(interrogation->availableAnswers());
		
		for (const auto& proposedAnswer: solutions)
		{
			// aide pour trouver la réponse plus facilement (à enlever un jour)
			auto answer  = proposedAnswer.first;
			if (proposedAnswer.second == "Correct")
			{
				answer += "<----";
			}
			
			auto* button = new QPushButton(QString::fromStdString(answer));
			connect(button, &QPushButton::clicked, this,
					[this, proposedAnswer]
					{ emit responseSelected(proposedAnswer.second); }
					);
			solutionLayout->addWidget(button);
		}

		mainLayout->addLayout(solutionLayout);
		setLayout(mainLayout);

	}
	
	std::vector<std::pair<std::string, NomCool::data::Response>>  Interrogation::shuffleAnswersExceptLast(const std::vector<std::pair<std::string, NomCool::data::Response>>& availableAnswers) {
    
		std::vector<std::pair<std::string, NomCool::data::Response>>  shuffledAnswers = availableAnswers;
		std::random_device rd;
		std::mt19937 g(rd());

		if (shuffledAnswers.size() > 1) {
			std::shuffle(shuffledAnswers.begin(), shuffledAnswers.end() - 1, g);
		}

		return shuffledAnswers;
	}
} // namespace NomCool::gui
